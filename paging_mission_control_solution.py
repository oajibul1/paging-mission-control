#!/usr/bin/python3
import pandas             #import pandas module to use for data manipulation
import json
from datetime import timedelta

#Column names of the input data
columns = ['timestamp', 'satellite-id', 'red-high-limit', 'yellow-high-limit', 'yellow-low-limit', 'red-low-limit', 'raw-value', 'component']

#Read in the data
input_data = pandas.read_csv("input.txt", sep='|', names=columns)

#Convert timestamp column to a datetime object
input_data['timestamp'] = pandas.to_datetime(input_data['timestamp'])

#Get list satellite ids
satellite_ids = input_data['satellite-id'].unique()

#Empty dictionary to store alerts for each satellite_id
id_alerts = {}
for id in satellite_ids:
    id_alerts[id] = []


#This will store the timestamp for matches of the requirement for each component
alerts_BATT = []
alerts_TSTAT = []

#Get the first time stamp
firstRow = input_data.iloc[0]
initial_timestamp = firstRow['timestamp']

#Time interval is 5 minutes
delta = timedelta(minutes=5)

for column, row in input_data.iterrows():
    within_interval = (row['timestamp'] - initial_timestamp) <= delta
    if not within_interval:
        for id in id_alerts.keys():
            id_alerts[id].clear()
        initial_timestamp = row['timestamp']
        
    if (row['raw-value'] < row['red-low-limit'] and row['component'] == 'BATT'):
        alerts = id_alerts[row['satellite-id']]
        alerts.append(row)
        batt_alerts = [x for x in alerts if x['component'] == 'BATT']
        if (len(batt_alerts) == 3):
            alerts_BATT.append(batt_alerts[0])
            alerts = [x for x in alerts if x['component'] != 'BATT']
            
    elif (row['raw-value'] > row['red-high-limit'] and row['component'] == 'TSTAT'):
        alerts = id_alerts[row['satellite-id']]
        alerts.append(row)
        tstat_alerts = [x for x in alerts if x['component'] == 'TSTAT']
        if (len(tstat_alerts) == 3):
            alerts_TSTAT.append(tstat_alerts[0])
            alerts = [x for x in alerts if x['component'] != 'TSTAT']
 
alert_messages=[]           
for row in alerts_TSTAT:
    alert_messages.append({'satelliteId':row['satellite-id'],
                           'severity':"RED HIGH",
                           'component':"TSTAT",
                           'timestamp':row['timestamp'].strftime('%Y-%m-%dT%H:%M:%S:%f')[:-3]+'Z'})

for row in alerts_BATT:
    alert_messages.append({'satelliteId':row['satellite-id'],
                           'severity':"RED LOW",
                           'component':"BATT",
                           'timestamp':row['timestamp'].strftime('%Y-%m-%dT%H:%M:%S:%f')[:-3]+'Z'})

#Alert messages JSON data dump 
alert_messages_json=(json.dumps(alert_messages,indent=4, default=str))
print(alert_messages_json)
      
